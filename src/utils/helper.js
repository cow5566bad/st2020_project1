
let sorting = (array) => {
    array.sort()
    return array;
}

let compare = (a, b) => {
    let pm25_a = Number(a['PM2.5'])
    let pm25_b = Number(b['PM2.5'])
    if (pm25_a == pm25_b)
        return 0
    return pm25_a < pm25_b ? -1 : 1
}

const reducer = (accumulator, currentValue) => accumulator + currentValue
let average = (nums) => {
    let avg = nums.reduce(reducer)/nums.length
    return Number((avg).toFixed(2))
}


module.exports = {
    sorting,
    compare,
    average
}
